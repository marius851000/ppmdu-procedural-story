from gameManager import gameManager
from pre import preGenerate
from debugEventRender import debugEventRender
from src2game.toXML import toXML
from src2game.fullXML import fullXML
# procedural story generator like pokemon mystery dungeon.
#
#
# Ce programme vas générer toute une histoire dans un monde rempli de pokémon
# un peut comme la série donjon mystére de nintendo ( et quelque autre acteur ).
#
# fonctionnement :
#  le programme procéde en plusieur étape :
#   Premierement : génére la situation initale ( quel PKMN, où, quel quette,
#       quel est l'objectif final... ) ( pre.py )
#   Seconde étape : générer ce qui vas se passer tout les jour ( gameManager.py )

def simulate():
    world = preGenerate()
    game = gameManager(world[0], world[1], world[2], verbose = False)
    a = game.go()
    debugEventRender(a)
    print(fullXML(toXML(a[3][0][0]), "G01P01A"))


if __name__ == '__main__':
    simulate()
