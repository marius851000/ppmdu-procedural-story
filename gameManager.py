from listSort import listSort

class gameManager:
    def __init__(self,PlaceManager,PKMNManager,QuestManager,verbose = False):
        self.PlaceManager = PlaceManager
        self.PKMNManager = PKMNManager
        self.QuestManager = QuestManager

        self.verbose = verbose

        self.day = 0

    def execute(self,quest):
        """execute une quete ( redirection vers quest manager )"""
        a = self.QuestManager.execute(quest,self)
        if quest.isFinal:
            self.finished = True
        if quest.timeNeed != 0:
            self.where = 0
            self.moment = self.moment + quest.timeNeed
        return a

    def go(self):
        """simulation.
        avoir une liste de quete à executé : [[le matin],[journé],[le soir]]
        le matin et le soir serve à avoir des séquence cinématique totalement scripté ( sans donjon )
        pour ce qui est de la journé, c'est la quéte qui l'occupe ( il peut en avoir plusieur ),
        et exceptionnelement ne pas prendre toute la journée.
        chaque jour:
            remettre à 0 la liste
            AJOUTER à la liste des quéte, les quete du matin, de la journé et du soir ( selement celle dont les condition le permettent)
            les trier par priorité
            s'il n'y a pas de quete dans la journé, en générer une
            s'il n'y a toujours pas de quéte de la journé, signaler une erreur
            les executé dans l'ordre. certaine quete peuvent être passé car elle prennent plus que le temps défini."""
        if self.verbose:
            print("-----------------------------------------------------------")
            print("début de la simuation du monde")
        self.rendu = []
        self.finished = False
        self.resume = []

        while self.finished != True and self.day < 30:
            self.day = self.day + 1
            self.rendu.append([[],[],[]])
            quete = [[],[],[]]#matin, journé, soir
            # ajout des quete
            self.QuestManager.check(self)

            if len(quete[1]) == 0:
                pass # TODO: générer une quete de maniére procedurale

            #if len(quete[1]) == 0: # pas de quete qui occupe la journé
            #    raise
            # TODO: reanable ( for debug )

            if self.verbose:
                print(quete)

            self.moment = 0 # 0 : matin, 1 : journé, 2 : soir, 3 : c'est fini
            self.where = -1
            while self.moment != 3:
                self.where = self.where + 1
                self.passed = False
                # TODO: il faut vérifier si les quete son toujours fesable ( les personnage nessaire ne sont pas mort, la situation n'est pas résulu par un autre moyen... ( rempli par les nessaicité ))
                #matin
                self.qexec(0)
                #journée
                self.qexec(1)
                #soir
                self.qexec(2)

        return self.rendu

    def qexec(self,nb):
        quete = self.QuestManager.findall(done = False, moment = nb, avalaible = True, GM = self)
        quete = listSort(quete,"priority")

        if self.moment == nb:
            if len(quete) > self.where:
                a = self.execute(quete[self.where])
                self.rendu[self.day-1][nb].append(a)
            else:
                self.moment = self.moment+1
                self.where = 0
