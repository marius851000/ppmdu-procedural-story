from randomList import randomList
from tabulate import tabulate

class MBTI:# TODO: il me semble qu'il y ait des copyright sur le nom de MBTI. verifier cela
    def __init__(self, energie = None, info = None, decision = None, mode = None):
        self.energie = energie # True = interieur, False = exterieur
        self.info = info # True = intuition, False = sensation
        self.decision = decision # True = pensée, False = sentiment
        self.mode = mode # True = jugement, False = Perception

    def generate(self):
        # TODO: tenir compte des statistique
        self.energie = randomList([True,False])
        self.info = randomList([True,False])
        self.decision = randomList([True,False])
        self.mode = randomList([True,False])

    def dump(self):
        rendu = ""
        rendu = rendu + "energie : "
        if self.energie:
            rendu = rendu + "interieur\n"
        else:
            rendu = rendu + "exterieur\n"
        rendu = rendu + "information : "
        if self.info:
            rendu = rendu + "intuition\n"
        else:
            rendu = rendu + "sensation\n"
        rendu = rendu + "decision : "
        if self.decision:
            rendu = rendu + "pensée\n"
        else:
            rendu = rendu + "sentiment\n"
        rendu = rendu + "mode : "
        if self.mode:
            rendu = rendu + "jugement"
        else:
            rendu = rendu + "perception"
        return tabulate(rendu)
