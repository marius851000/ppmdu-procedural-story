def tabulate(texte):
    """return texte avec une tabulation en plus à chaque ligne"""
    rendu = ""
    for loop in texte.split("\n"):
        rendu = rendu + "\t" + loop + "\n"
    rendu = rendu[0:len(rendu)-1]
    return rendu
