class questBase:
    def __init__(self,PKMNManager,PlaceManager,QuestManager):
        """une quete. Si un le jeu veut forcer la quete à avoir des spécificité,
        c'est avec generate qu'il faut voir"""
        self.done = False # fini ?
        self.PlaceManager = PlaceManager # le gestionnaire de lieu global
        self.PKMNManager = PKMNManager # le gestionnaire de pokémon global
        self.QuestManager = QuestManager
        self.priority = 100 # la priority. le plus petit = le moins important ( peut être négatif )
        # si supérieur à 500, executé même s'il y a des séquence spéciale ( quest.check() )
        # quelque valeur : 1000 = la fin
        # 2000 = l'introduction
        self.name = "unk"
        self.resume = "pas de résumé disponible"
        self.moment = 1
        self.isFinal = False
        self.prerequisite = [] # les prerequi ( sous forme de la classe prérequi)
        self.timeNeed = 0 # 0 = aucun
        # debut  |
        # matin  | 0     | 1      | 2    | 3
        # journé |       | 0      | 1    | 2
        # soir   |       |        | 0    | 1
        # arrivé | matin | journé | soir | nuit
        self.postInit()

    def checkPreRequisite(self,GM):
        for loop in self.prerequisite:
            if loop.check(GM) == False:
                return False
        return True

    def postInit(self):
        pass

    def generate(self):# should be overwritted
        raise

    def check(self):#called every day to see if there should not have scripted sequence
        return False

    def dump(self):
        rendu = ""
        rendu = rendu + "name : " + self.name + "\n"
        rendu = rendu + "resume : " + self.resume
        return rendu

    def execute(self,GM):
        #execute la quete
        self.rendu = {"name" : self.name,
            "resume" : self.resume,
            "self" : self}
        self.postExecute(GM)
        self.done = True
        return self.rendu

    def postExecute(self,GM):
        pass
