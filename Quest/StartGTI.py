from Quest.questBase import questBase

class StartGTI(questBase):
    """génere une quéte initiale similaire à celle de Gate to infinite
    ( partenaire nous voit et nous améne la ou il habite )"""
    def postInit(self):
        self.name = "début similaire à GTI"
        self.priority = 2000
        self.timeNeed = 1

    def generate(self):
        self.partner = self.PKMNManager.findall(isPartner = True)[0] # le partnaire
        self.player = self.PKMNManager.findall(isPlayer = True)[0] # le joueur
        self.lieu = self.player.location
        self.whereToGo = self.partner.inhabit

        self.shouldGoHome = not(self.lieu == self.whereToGo)

        self.shouldGoHome

        self.name = self.partner.name + " trouve " + self.player.name + "."
        self.resume = self.player.name + " est transformé en pokémon. il \
retrouve ses esprit dans un(e) " + self.player.location.placeType.name + ". Alors qu'\
il se remet de ses émotion, " + self.partner.name + " le trouve. comme " + self.player.name + "\
 n'a nul par ou aller, il accepte."
        if self.shouldGoHome:
            self.resume = self.resume + " " + self.partner.name + " \
l'accompagne donc chez lui/elle."
