from Quest.questBase import questBase
from prerequisite.preDay import preDay

class MFinalDARKMATTERAnnonce(questBase):
    """le grand méchant s'étant sur le monde et annonce qu'il vas pétrifier tout le monde"""
    def postInit(self):
        self.name = "je vais vous transformer en pierre"
        self.moment = 0
        self.prerequisite = [preDay(3,"<")]

    def generate(self,FinalDARKMATTER):
        self.FinalDARKMATTER = FinalDARKMATTER
        self.name = self.FinalDARKMATTER.evil.name + " signale à tous le monde qu'il vas les pétrifier."

    def postExecute(self,GM):
        self.rendu["action"] = [self.executeTest(GM)]

    def executeTest(self,GM):
        player = self.PKMNManager.findall(isPlayer = True)[0]
        partner = self.PKMNManager.findall(isPartner = True)[0]
        el = {}
        el["here"] = []
        el["here"].append(player.element(x=10,y=10))
        el["here"].append(partner.element(x=10,y=11))
        el["where"] = "G01P01A"
        el["action"] = [
        {
        "function" : "Function",
        "element" :
            [
            {
                "command" : "sound_Stop",
                "param" : None
            },{
                "command" : "message_Talk",
                "param" : {
                    "string" : {
                        "English" : "I'm an evil, mouhahahaha.",
                        "French" : "Je suis un méchant, mouhahahahah."
                    }
                }
            }
            ]

        }
        ]
        return el
