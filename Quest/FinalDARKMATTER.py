from Quest.questBase import questBase
from Quest.MFinalDARKMATTERAnnonce import MFinalDARKMATTERAnnonce
from pokemon import pokemon
from prerequisite.preQuest import preQuest
import random

class FinalDARKMATTER(questBase):
    """génere une quete final similaire à celle de pokemon méga donjon
    mystére"""
    def postInit(self):
        self.name = "combat contre le grand méchant final"
        self.isFinal = True
        self.priority = 1000
        self.timeNeed = 1
        self.step = 0 # ou en est on dans l'histoire

    def generate(self,evil = None, what = None):
        """evil : le grand méchant pokémon ( classe )
        what : comment le méchant veut détruire le monde ( int )
            0 : l'envoyer vers le soleil
            1 : le faire exploser
            2 : l'écraser sur lui même ( colleapse )"""
        if evil == None: # le grand méchant boss final
            evil = pokemon(self.PlaceManager)
            evil.PKMNManager = self.PKMNManager
            evil.generate()
            evil.postGenerate(friend = False)
            self.PKMNManager.allPKMN.append(evil)

        if what == None:# comment le monde est détruit :)
            what = random.randint(0,2)

        self.what = what # whatwhat ?
        self.whatSTR = "le détruisant avec une erreur de mauvais argument"
        if what == 0:
            self.whatSTR = "l'envoyant vers le soleil"
        elif what == 1:
            self.whatSTR = "le faisant exploser"
        elif what == 2:
            self.whatSTR = "le faisant écraser sur lui même"


        self.evil = evil
        self.name = "combat contre " + self.evil.name + "."
        self.resume = self.evil.name + " pétrifie des pokémon pour se remplir d'énergie, afin d'exterminer le monde en " + self.whatSTR + ". " + self.PKMNManager.findall(isPlayer = True)[0].name + " et " + self.PKMNManager.findall(isPartner = True)[0].name + " le découvre, trouve un moyen d'aller le voir, et l'affronte."

        self.announce = MFinalDARKMATTERAnnonce(self.PKMNManager,self.PlaceManager,self.QuestManager)
        self.announce.generate(self)
        self.QuestManager.addQuest(self.announce)
        self.prerequisite = [preQuest(self.announce)]
