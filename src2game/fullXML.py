def fullXML(code, area):
    allCode = []
    for loop in code:
        if loop["where"] == area:
            allCode.append(loop)

    rendu = ""
    rendu = rendu + "<?xml version=\"1.0\"?>"
    rendu = rendu + '\n<Level gameVersion="EoS" gameRegion="Europe">'
    #generation des LSDTable
    rendu = rendu + '\n<LSDTable>'
    for loop in allCode:
        name = loop["LSD"]
        if name != "enter":
            rendu = rendu + '\n<Ref name="' + loop["LSD"] + '">'
    rendu = rendu + '\n</LSDTable>'
    for loop in allCode:
        rendu = rendu + loop["code"]
    rendu = rendu + '\n</Level>'

    return rendu
