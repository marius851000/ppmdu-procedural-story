from tabulate import tabulate



def toXML(a):
    rendu = []
    action = a["action"]
    for loop in action:# pour chaque zone
        retour = actionToXML(loop,a)
        rendu.append({
            "where" : loop["where"],
            "code" : retour[0],
            "LSD" : retour[1]})
    return rendu

def actionToXML(b,a): # gestion des zone
    rendu = ""
    # TODO:
    scriptName = "enter"
    scriptType = "sse"
    rendu = rendu + '\n<ScriptSet name="'+scriptName+'">'
    rendu = rendu + '\n<ScriptData name="'+scriptName.lower()+'" type="'+scriptType+'">'
    here = b["here"]
    rendu = rendu + heresToXML(here,a)
    rendu = rendu + '\n</ScriptData>'
    rendu = rendu + '\n<ScriptSequence name="'+scriptName.lower()+'00">'
    rendu = rendu + '\n<Code>'
    code = b["action"]
    for loop in code:
        rendu = rendu + codeToXML(loop,a)

    rendu = rendu + '\n</Code>'
    rendu = rendu + '\n</ScriptSequence>'
    rendu = rendu + '\n</ScriptSet>'
    return [rendu,scriptName]

def codeToXML(b,a):
    rendu = ""
    # TODO: gestion des id
    if b["function"] == "Function":
        rendu = rendu + '\n<Function _id="0">'
    code = b["element"]
    for c in code:
        tag = c["command"]
        param = c["param"]
        if tag == "message_Talk":
            rendu = rendu + "\n<message_Talk>"
            rendu = rendu + stringToXML(param["string"])
            rendu = rendu + "\n</message_Talk>"
        else: #gestion par défaut
            paramSTR = ""
            if param != None:
                pass # TODO: gestion des paramétre
            rendu = rendu + "\n<"+tag+" "+paramSTR+" />"

    if b["function"] == "Function":
        rendu = rendu + '\n</Function>'
    return rendu

def stringToXML(string):
    rendu = ""
    done = []
    for loop in string:
        rendu = rendu + "\n<String language=\"" + loop + "\">" + string[loop] + "</String>"
        done.append(loop)
    if not "French" in done:
        rendu = rendu + "\n<String language=\"French\">"+string["English"] + "</String>"
    if not "German" in done:
        rendu = rendu + "\n<String language=\"German\">"+string["English"] + "</String>"
    if not "Italian" in done:
        rendu = rendu + "\n<String language=\"Italian\">"+string["English"] + "</String>"
    if not "Spanish" in done:
        rendu = rendu + "\n<String language=\"Spanish\">"+string["English"] + "</String>"

    return rendu

def heresToXML(here,a):# gestion des acteur de la scene
    rendu = ""
    #les trier par layer
    # TODO: trie par layer
    layer = [None]*100
    layer[0] = here
    #les trier par type
    #actor, object, performer
    layer2 = []
    for loop in range(100):
        layer2.append([[],[],[]])
    layer2[0][0] = layer[0]
    # TODO: trie par type
    layer = layer2
    rendu = rendu + "\n<Layers>"

    for loop in range(len(layer)):#pour chaque layer

        ceLayer = layer[loop]
        if len(ceLayer[0])!=0 or len(ceLayer[1]) != 0 or len(ceLayer[2]) != 0:
            rendu = rendu + "\n<Layer _id=\""+str(loop)+"\">"
            for loop2 in range(len(ceLayer)):#pour chaque type d'élement du layer
                ceType = ceLayer[loop2]
                if len(ceType) != 0:
                    if loop2 == 0:
                        Type = "Actor"
                    elif loop2 == 1:
                        Type = "Object"
                    elif loop2 == 2:
                        Type = "Performer"
                    else:
                        raise

                    rendu = rendu + "\n<" + Type + "s>"
                    for loop3 in ceType: #chaque personnage:
                        rendu = rendu + actorToXML(loop3,a)
                    rendu = rendu + "\n</" + Type + "s>"

            rendu = rendu + "\n</Layer>"

    rendu = rendu + "\n</Layers>"
    return rendu

def actorToXML(b,a):
    if b["type"] == "Actor":
        rendu = '\n<Actor actorid="'+b["actorid"]+'" facing="'+b["facing"]+'" x="'+str(b["x"])+'" y="'+str(b["y"])+'" unk4="'+b["unk4"]+'" unk5="'+b["unk5"]+'" act_scriptid="'+"-1"+'" />'
    return rendu
