from PKMNType import PKMNType
import random
from randomName import randomName
from randomList import randomList
from tabulate import tabulate
from relation.friend import friendRelation
from MBTI import MBTI

class pokemon:
    # TODO: ajouter un " ce pokemon sait x sur y "
    def __init__(self,placeManager, player = False, partner = False):
        self.location = None # ou se trouve t'il en ce moment
        self.inhabit = None # ou habite t'il
        self.name = "" # son nom
        self.MBTI = MBTI() # le MBTI du pokémon
        self.placeManager = placeManager # le gestionnaire de lieu
        self.PKMNManager = None # le gestionnaire de pokémon

        self.isPlayer = player # est ce que c'est le joueur ?
        self.isPartner = partner # est ce que c'est le partenaire

        self.type = PKMNType(1) # qu'el est la race du pokémon

        self.relation=[] # les relation, sous forme de la classe relation
        self.sex = 0 # 0 = indéfini, 1 = male, 2 = femelle
        self.live = True # le pokémon est t'il vivant ?
        self.dieable = False # TODO:


    def generate(self,shouldBeAt=None):
        self.sex = random.randint(1,2)
        self.type = PKMNType(random.randint(1,720)) # TODO: amiliorer cette fonction
        self.name = randomName()
        self.MBTI.generate()
        #habitation
        if not self.isPlayer:
            self.inhabit = randomList(self.placeManager.listall(livable = True))
        #localisation
        if shouldBeAt == None:
            if random.randint(0,1)==0 and self.inhabit != None:#est dans son lieu d'habitation
                self.location = self.location
            else:
                self.location = randomList(self.placeManager.listall())
        else:
            self.location = shouldBeAt

    def postGenerate(self, friend = True):
        if self.PKMNManager == None:
            raise
        if friend:
            for loop in range(random.randint(1,2)):
                # TODO: rendre possible quelqun sans amis ( ex : partenaire )
                self.makeFriendRandom()

    def makeFriendRandom(self):
        self.addRelation(friendRelation,randomList(self.PKMNManager.findall(isPlayer=False)))

    def addRelation(self,relation,pokemon,isReciprocal=None):
        newRelation = relation(self,pokemon,isReciprocal)
        if newRelation.isReciprocal == True:
            newRelation2 = pokemon.addRelation(relation,self,isReciprocal=False)
            newRelation.otherLink = newRelation2
            newRelation2.otherLink = newRelation
        self.relation.append(newRelation)
        return newRelation

    def element(self, layer = 0, facing = "Up", x = 0, y = 0, unk4 = "0x0", unk5 = "0x2", act_scriptid = -1 ):
        """pour une inclusion dans un script"""
        rendu = {}
        rendu["type"] = "Actor"
        rendu["layer"] = layer
        if self.isPlayer:
            rendu["actorid"] = "PLAYER"
        elif self.isPartner:
            rendu["actorid"] = "ATTENDANT1"
        else:
            rendu["actorid"] = "NPC_DARK_PUKURIN"
            # TODO: error
        rendu["facing"] = facing
        rendu["x"] = x
        rendu["y"] = y
        rendu["unk4"] = unk4
        rendu["unk5"] = unk5
        return rendu

    def dump(self):
        rendu = ""
        if self.location == None :
            rendu = rendu + "location : None\n"
        else:
            rendu = rendu + "location : " + self.location.name + "\n"
        if self.inhabit == None :
            rendu = rendu + "inhabit : None\n"
        else:
            rendu = rendu + "inhabit : " + self.inhabit.name + "\n"
        rendu = rendu + "name : " + self.name + "\n"
        rendu = rendu + "sex : " + str(self.sex) + "\n"
        rendu = rendu + "isPartner : " + str(self.isPartner) + "\n"
        rendu = rendu + "isPlayer : " + str(self.isPlayer) + "\n"
        rendu = rendu + "type : " + str(self.type.race) + "\n"
        rendu = rendu + "MBTI : " + self.MBTI.dump() + "\n"
        rendu = rendu + "relation : \n"
        for loop in self.relation:
            rendu = rendu + "\trelation :\n"
            rendu = rendu + tabulate(loop.dump())+"\n"
        return tabulate(rendu)
