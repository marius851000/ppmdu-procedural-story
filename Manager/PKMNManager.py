from tabulate import tabulate

class PKMNManager:
    def __init__(self,allPKMN, placeManager):
        self.allPKMN = allPKMN
        self.placeManager = placeManager

    def findall(self, isPlayer = None, isPartner = None):
        rendu = []
        for loop in self.allPKMN:
            selected = True
            if isPlayer != None:
                if isPlayer != loop.isPlayer:
                    selected = False
            if isPartner != None:
                if isPartner != loop.isPartner:
                    selected = False
            if selected:
                rendu.append(loop)
        return rendu

    def dump(self):
        rendu = ""
        rendu = rendu + "PKMNManager :\n"
        for loop in self.allPKMN:
            rendu = rendu + "\tpokemon :\n"
            rendu = rendu + tabulate(loop.dump()) + "\n"
        return rendu
