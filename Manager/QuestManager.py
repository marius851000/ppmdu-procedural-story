from tabulate import tabulate

class QuestManager:
    def __init__(self,PKMNManager,PlaceManager):
        self.PKMNManager = PKMNManager
        self.PlaceManager = PlaceManager
        self.allQuest = []

    def addQuest(self,quest):
        self.allQuest.append(quest)

    def execute(self,quest,GM):
        a = quest.execute(GM)
        quest.done = True
        return a

    def findall(self,done = None,moment = None, avalaible = None, GM = None):
        rendu = []
        for loop in self.allQuest:
            itok = True
            if done != None:
                if done != loop.done:
                    itok = False
            if moment != None:
                if moment != loop.moment:
                    itok = False
            if avalaible != None:
                if avalaible != loop.checkPreRequisite(GM):
                    itok = False
            if itok:
                rendu.append(loop)
        return rendu

    def check(self,GM):
        sequence = []
        for loop in self.allQuest:
            a = loop.check()
            if a != False:
                sequence.append(a)
                for loop in a:
                    self.allQuest.append(loop)

    def dump(self):
        rendu = ""
        rendu = "QuestManager :\n"
        for loop in self.allQuest:
            rendu = rendu + "\tquete :\n"
            rendu = rendu + tabulate(tabulate(loop.dump())) + "\n"
        return tabulate(rendu)
