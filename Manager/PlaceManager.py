from tabulate import tabulate

class PlaceManager:
    """ gére les différent lieu """
    def __init__(self,place):
        self.allPlace = place

    def check(self):
        if len(self.listall(livable = True)) == 0:
            #pas de place vivable
            raise
        if len(self.listall(canBeStartPoint = True)) == 0:
            #pas de point de départ possible
            raise

    def listall(self,livable = None, canBeStartPoint = None):
        liste = []
        for loop in self.allPlace:
            avalaible = True
            if livable != None:
                if loop.placeType.livable != livable:
                    avalaible = False

            if canBeStartPoint != None:
                if loop.placeType.canBeStartPoint != canBeStartPoint:
                    avalaible = False

            if avalaible:
                liste.append(loop)
        return liste

    def dumpPlace(self, places):
        rendu = ""
        for loop in places:
            rendu = rendu + "\tplace :\n"
            rendu = rendu + tabulate(loop.dump()) + "\n"
        return rendu
    def dump(self):
        """info de debug sous forme de string"""
        rendu = "PlaceManager :\n"
        rendu = rendu + self.dumpPlace(self.allPlace)
        return tabulate(rendu)
