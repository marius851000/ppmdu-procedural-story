from tabulate import tabulate
import random

# la class placeType contient les diffirent type de lieu ainssi que placeRandom
# qui selectionne un type de lieu aléatoire

def placeTypeRandom():
    # TODO:
    aleatoire = random.randint(0,1)
    if aleatoire == 1:
        return villageType
    else:
        return forestType

class defaultType:
    """un type par default.
    pour ajouter des chose aprés le démarage, modifier la fonction postInit()
    ( ne pas oublier d'hériter )"""
    def __init__(self):
        self.preInit()
        self.name = "defaultType"
        self.canBeStartPoint = False
        self.livable = False
        self.postInit()

    def preInit(self):
        pass

    def postInit(self):
        pass

    def dump(self):
        """returne un resultat des principaux element sous forme de string.
        pour le débugage"""
        rendu = "name : " + self.name + "\n"
        rendu = rendu + "can be start point : " + str(self.canBeStartPoint) + "\n"
        rendu = rendu + "livable : " + str(self.livable)
        return tabulate(rendu)

class villageType(defaultType):
    """ un village """
    def postInit(self):
        self.name = "village"
        self.canBeStartPoint = True
        self.livable = True

class forestType(defaultType):
    """ une foret """
    def postInit(self):
        self.name = "foret"
        self.canBeStartPoint = True
        self.livable = False
