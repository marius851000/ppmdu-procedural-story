from placeGenerate import preGenerate as placePreGenerate
from Manager.PlaceManager import PlaceManager
from PKMNGenerate import preGenerate as PKMNPreGenerate
from Manager.QuestManager import QuestManager
from generateQuest import generateFinalQuest, generateStartQuest
# prégenere le monde. procéde en trois étape :
#  1:
#   génére les diffént lieu ou l'avanture se déroute ( placeGenerate.py - func preGenerate )
#  2:
#   génére les différent pokémon ( PKMNGenerate.py - func preGenerate )
#  3:
#   génére l'objectif final ( contennu dans questGenerate.py - fonction final )

def preGenerate():
    """pre-génére le monde.
    retourne un tableau.
    Le tableau 0 contient tous les lieu
    Le tableau 1 contient tous les pokémon
    La classe 2 contient le quest manager
    /!\, ne pas oublier de tout mettre dans PlaceManager / PKMNManager / QuestManager"""
    #genere les lieu
    allPlace = placePreGenerate()
    place = PlaceManager(allPlace)
    place.check()
    PKMN = PKMNPreGenerate(place)
    QM = QuestManager(PKMN,place)
    QM.addQuest(generateFinalQuest(PKMN,place,QM))
    QM.addQuest(generateStartQuest(PKMN,place,QM))
    return [place,PKMN,QM]

if __name__ == '__main__':
    rendu = preGenerate()
    print(rendu[0].dump())
    print(rendu[1].dump())
    print(rendu[2].dump())
