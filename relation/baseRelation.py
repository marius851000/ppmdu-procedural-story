from tabulate import tabulate
class baseRelation:
    # TODO: retravailler pour faire en sorte à ce qu'il puisse y avoir une
    # infinité de personne géré par la relation ( cf to = tableau)
    def __init__(self,frome,to, isReciprocal = None):
        self.frome = frome # le pokemon à l'origine de la relation
        self.to = to # le pokemon à qui la relation s'addresse
        self.isReciprocal = False
        self.name = "unk"
        self.postInit()
        if isReciprocal == False:
            self.isReciprocal = False
        elif isReciprocal == True:
            self.isReciprocal = True
        self.otherLink = None # la relation pour l'autre ( si reciproque )

    def postInit(self):
        pass

    def dump(self):
        rendu = ""
        rendu = rendu + "name : " + self.name + "\n"
        rendu = rendu + "frome : " + self.frome.name + "\n"
        rendu = rendu + "to : " + self.to.name
        return tabulate(rendu)
