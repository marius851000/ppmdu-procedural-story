from tabulate import tabulate

def debugEventRender(event):
    # TODO: cleanup
    rendu = "présentation de l'histoire généré procéduralement par PMD procedural story generator"
    for loop in range(len(event)):
        eventToday = event[loop]
        day = loop + 1
        renduJour = ""

        if day == 1:
            renduJour = renduJour + "\nle premier jour :"
        elif day == 2:
            renduJour = renduJour + "\nle second jour :"
        else:
            renduJour = renduJour + "\nle " + str(day) + "éme jour :"

        for moment in range(len(eventToday)):
            renduBoucle = ""
            eventMoment = eventToday[moment]
            if len(eventMoment) != 0:

                if moment == 0:
                    renduBoucle = renduBoucle + "\nle matin :"
                elif moment == 1:
                    renduBoucle = renduBoucle + "\nla journée :"
                elif moment == 2:
                    renduBoucle = renduBoucle + "\nle soir :"

                for quete in eventMoment:
                    renduBoucle = renduBoucle + "\n\tquete :"
                    renduBoucle2 = ""
                    renduBoucle2 = renduBoucle2 + "\nnom : " + quete["name"]
                    renduBoucle2 = renduBoucle2 + "\nrésumé : " + quete["resume"]
                    renduBoucle2 = renduBoucle2 + "\nclasse : " + str(quete["self"])
                    renduBoucle = renduBoucle + tabulate(tabulate(renduBoucle2))

                renduJour = renduJour + tabulate(renduBoucle)
        rendu = rendu + renduJour
    print(rendu)
