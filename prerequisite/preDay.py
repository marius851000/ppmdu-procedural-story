from prerequisite.preBase import preBase

class preDay(preBase):
    """param1 : le jour
    param2 : le test ( param param2 day) ( eg 1 > day )
    param2 en str"""
    def check(self,GM):
        day = GM.day
        if self.param2 == "==":
            if self.param1 == day:
                return True
            else:
                return False
        elif self.param2 == ">":
            if self.param1 > day:
                return True
            else:
                return False
        elif self.param2 == "<":
            if self.param1 < day:
                return True
            else:
                return False
        return False
