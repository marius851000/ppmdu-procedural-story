from prerequisite.preBase import preBase

class preQuest(preBase):
    """param1 = la quete qu'il faut avoir fini"""
    def check(self, GM):
        return self.param1.done
