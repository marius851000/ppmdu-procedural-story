
def generateStartQuest(PKMNManager,PlaceManager,QM):
    # TODO: add more start quest
    from Quest.StartGTI import StartGTI
    return generateQuest(StartGTI,PKMNManager,PlaceManager,QM)

def generateFinalQuest(PKMNManager,PlaceManager,QM):
    # TODO: add more final quest
    from Quest.FinalDARKMATTER import FinalDARKMATTER
    return generateQuest(FinalDARKMATTER,PKMNManager,PlaceManager,QM)


def generateQuest(quete, PKMNManager,PlaceManager,QM):
    quest = quete(PKMNManager,PlaceManager,QM)
    quest.generate()
    return quest
