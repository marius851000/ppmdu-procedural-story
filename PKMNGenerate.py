from pokemon import pokemon
from Manager.PKMNManager import PKMNManager

def preGenerate(placeManager):
    allPKMN = []
    PKMNTag = {"normal" : []}
    # generate player
    player = pokemon(placeManager, player = True)
    player.generate()
    allPKMN.append(player)
    PKMNTag["player"] = player

    # generate partner
    partner = pokemon(placeManager, partner = True)
    partner.generate(shouldBeAt=player.location)
    allPKMN.append(partner)
    PKMNTag["partner"] = partner



    # generate other
    for loop in range(30):
        newPKMN = pokemon(placeManager)
        newPKMN.generate()
        allPKMN.append(newPKMN)
        PKMNTag["normal"].append(newPKMN)

    # put all in PKMNManager
    PKMNM = PKMNManager(allPKMN,placeManager)


    # post generation

    for loop in allPKMN:
        loop.PKMNManager = PKMNM
        loop.postGenerate()

    return PKMNM
