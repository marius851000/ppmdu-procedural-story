from placeType import placeTypeRandom
from randomName import randomName
from tabulate import tabulate

class Place:
    def __init__(self):
        self.connectedTo = []
        self.name = "unk"
        self.placeType = None

    def generate(self):
        self.name = randomName()
        self.placeType = placeTypeRandom()()

    def dump(self):
        rendu = "name : " + self.name + "\n"
        rendu = rendu + "type :\n" + self.placeType.dump() + "\n"
        return tabulate(rendu)
